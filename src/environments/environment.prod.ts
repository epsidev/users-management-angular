export const environment = {
  production: true,

  authenticationApiURl: 'https://users.ws.montpellier.epsi.fr/api/auth',
  usersApiURl: 'https://users.ws.montpellier.epsi.fr/api/v1/users'
};
